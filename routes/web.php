<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//web controller bus
Route::get('/admin/bus/create', 'Bus\Web\ManageBus@CreateBus')->name('web.admin.bus.create');
Route::post('/admin/bus/create', 'Bus\Web\ManageBus@SaveBus')->name('web.admin.bus.save');
Route::get('/admin/bus/datatables/api', 'Bus\Web\ManageBus@Datatablesapi')->name('web.admin.bus.datatables.api');
Route::get('/admin/bus/all', 'Bus\Web\ManageBus@CreateBusall')->name('web.admin.bus.all');
//web controller bus End
//
/// //web controller bus
Route::get('/admin/student/create', 'Student\Web\ManageStudent@CreateStudent')->name('web.admin.student.create');
Route::post('/admin/student/create', 'Student\Web\ManageStudent@SaveStudent')->name('web.admin.student.save');
Route::get('/admin/student/datatables/api', 'Student\Web\ManageStudent@Datatablesapi')->name('web.admin.student.datatables.api');
Route::get('/admin/student/all', 'Student\Web\ManageStudent@CreateStudentall')->name('web.admin.student.all');
//web controller bus End


Route::get('/', function () {
    return view('admin.page.home.homepage');
});

Route::get('test', function () {
    // Route logic...
    $data = array("id" => "1", "name" => "jobin", "amount" => "1000");
    LRedis::publish('test-channel', json_encode($data));
    event(new App\Events\NewMessage($data));
    return "event fired";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('fire', function () {
    // this fires the event
    event(new App\Events\EventName());
    return "event fired";
});

