<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/user2', function (Request $request) {
    return $request->user('api');
});


Route::get('/admin', function (Request $request) {
    return $request->user('api');
});


//bus functionds //student-api
Route::post('bus/auth/register', 'bus\Auth@busRegister');
Route::get('bus/getdetails', 'Bus\ManageBus@getBusDetails');
Route::post('bus/sendlocation', 'Bus\ManageBus@PostLocation');
//bus functionds //
//
///
///
///   //student functionds //
Route::post('student/auth/register', 'Student\AuthController@studentRegister');


//students post
Route::middleware('auth:api')->get('student/auth/login', function (Request $request) {
    return $request->user('student-api');
});

Route::get('student/getdetails', 'Student\ManageStudent@getStudentsDetails');
Route::get('student/getbuslocation', 'Student\ManageStudent@getBusLocation');
//student functionds //