<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    | Of course, a great default configuration has been defined for you
    | here which uses session storage and the Eloquent user provider.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],


        'admin' => [
            'driver' => 'passport',
            'provider' => 'admins',
        ],
        'admin-api' => [
            'driver' => 'passport',
            'provider' => 'admins',
        ],



        'student' => [
            'driver' => 'passport',
            'provider' => 'students',
        ],
        'student-api' => [
            'driver' => 'passport',
            'provider' => 'students',
        ],



        'bus' => [
            'driver' => 'passport',
            'provider' => 'buses',
        ],
        'bus-api' => [
            'driver' => 'passport',
            'provider' => 'buses',
        ],




        'moderator' => [
            'driver' => 'passport',
            'provider' => 'moderators',
        ],
        'moderator-api' => [
            'driver' => 'passport',
            'provider' => 'moderators',
        ],




        'truck' => [
            'driver' => 'passport',
            'provider' => 'trucks',
        ],
        'truck-api' => [
            'driver' => 'passport',
            'provider' => 'trucks',
        ],



        'customers' => [
            'driver' => 'passport',
            'provider' => 'customers'
        ],



    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],
//next2
        'admins' => [
            'driver' => 'eloquent',
            'model' => App\Admin::class,
        ],

        //next
        'buses' => [
            'driver' => 'eloquent',
            'model' => App\Bus::class,
        ],

        //next
        'students' => [
            'driver' => 'eloquent',
            'model' => App\Student::class,
        ],

        //next
        'trucks' => [
            'driver' => 'eloquent',
            'model' => App\Truck::class,
        ],

        //next
        'moderators' => [
            'driver' => 'eloquent',
            'model' => App\Moderator::class,
        ],

        'customers' => [
            'driver' => 'eloquent',
            'model' => App\Customer::class,
        ],
        

        //next



        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | You may specify multiple password reset configurations if you have more
    | than one user table or model in the application and you want to have
    | separate password reset settings based on the specific user types.
    |
    | The expire time is the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed. You may change this as needed.
    |
    */

    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],

];
