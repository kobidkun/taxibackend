<?php

namespace App\Http\Controllers\Bus\Web;

use App\Bus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManageBus extends Controller
{
    public function CreateBus() {
        return view('admin.page.bus.create');
    }

    public function CreateBusall() {
        return view('admin.page.bus.all');
    }

    public function SaveBus(Request $request) {
       $a = new Bus();
       $a->name = $request->name;
       $a->bus_number = $request->bus_number;
       $a->driver_name = $request->driver_name;
       $a->email = $request->email;
       $a->password = bcrypt($request->password);
       $a->save();

       return redirect(route('web.admin.bus.create'));
    }


    public function Datatablesapi(Request $request) {
        $customers = Bus::select(
            [
                'id',
                'name',
                'driver_name',
                'email',
                'created_at',
            ])->orderBy('id', 'desc');

        return Datatables::of($customers)


            ->make(true);

    }
}
