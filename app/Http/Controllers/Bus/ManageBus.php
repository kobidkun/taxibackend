<?php

namespace App\Http\Controllers\Bus;


use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ManageBus extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:bus-api');
    }



    public function getBusDetails(Request $request)
    {
        $sd = $request->user();

        return response()->json(['bus' => $sd]);
    }

    public function PostLocation(Request $request)
    {
        $sd = $request->user();
        $getBusid = $sd->id;

        //post bus

        $sv = new Location();
        $sv->sender_id = '123';
        $sv->receiver_id = '456';
        $sv->bus_id = $sd->id;
        $sv->latitude = $request->lat;
        $sv->longitude = $request->long;
        $sv->speed = $request->speed;
        $sv->is_moving = 0;
        $sv->save();

        return response()->json($sv);







    }


}
