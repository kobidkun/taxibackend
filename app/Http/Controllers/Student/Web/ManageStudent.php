<?php

namespace App\Http\Controllers\Student\Web;

use App\Bus;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
class ManageStudent extends Controller
{
    public function CreateStudent() {
        $bus = Bus::all();
        return view('admin.page.student.create')->with([
            'bus' => $bus
        ]);
    }

    public function CreateStudentall() {
        $bus = Bus::all();

        return view('admin.page.student.all')->with([
            'bus' => $bus
        ]);
    }

    public function SaveStudent(Request $request) {



        $a = new Student();
        $a->name = $request->name;
        $a->class = $request->class;
        $a->roll = $request->roll;
        $a->section = $request->section;
        $a->stoppage = $request->stoppage;
        $a->blood_group = $request->blood_group;
        $a->mobile = $request->mobile;
        $a->email = $request->email;
        $a->street = $request->street;
        $a->locality = $request->locality;
        $a->city = $request->city;
        $a->bus_id = $request->bus_id;
        $a->password = bcrypt($request->password);
        $a->save();

        return redirect(route('web.admin.student.create'));
    }


    public function Datatablesapi(Request $request) {
        $customers = Student::select(
            [
                'id',
                'name',
                'class',
                'roll',
                'stoppage',
                'mobile',
                'bus_id',
                'email',
                'created_at',
            ])->orderBy('id', 'desc');

        return Datatables::of($customers)


            ->make(true);

    }
}
