<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\JWTAuth;

class UserDetails extends Controller
{
    public function GetUserProfile(JWTAuth $auth){

        $auth->parseToken();
        $user = $auth->toUser();

        return response()->json($user,200);
    }
}
