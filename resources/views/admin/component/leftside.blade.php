
<aside class="main-sidebar fixed offcanvas b-r sidebar-tabs" data-toggle='offcanvas'>
    <div class="sidebar">
        <div class="d-flex hv-100 align-items-stretch">
            <div class="indigo text-white">
                <div class="nav mt-5 pt-5 flex-column nav-pills" id="v-pills-tab" role="tablist"
                     aria-orientation="vertical">
                    <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab"
                       aria-controls="v-pills-home" aria-selected="true"><i class="icon-inbox2"></i></a>

                </div>
            </div>
            <div class="tab-content flex-grow-1" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                     aria-labelledby="v-pills-home-tab">
                    <div class="relative brand-wrapper sticky b-b">
                        <div class="d-flex justify-content-between align-items-center p-3">
                            <div class="text-xs-center">
                                <span class="font-weight-lighter s-18">Menu</span>
                            </div>

                        </div>
                    </div>
                    <ul class="sidebar-menu">


                        <li class="treeview"><a href="#"><i class="icon icon-user-circle s-24"></i>Students<i
                                        class=" icon-angle-left  pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('web.admin.student.all')}}"><i class="icon icon-circle-o"></i>All Students</a>
                                </li>


                                <li><a href="{{route('web.admin.student.create')}}"><i class="icon icon-circle-o"></i>Create Student</a>
                                </li>

                            </ul>
                        </li>
                        <li class="treeview"><a href="#"><i class="icon icon-bus s-24"></i>Buses<i
                                        class=" icon-angle-left  pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="{{route('web.admin.bus.all')}}"><i class="icon icon-circle-o"></i>All Buses</a>
                                </li>


                                <li><a href="{{route('web.admin.bus.create')}}"><i class="icon icon-circle-o"></i>Create Buses</a>
                                </li>

                            </ul>
                        </li>


                    </ul>
                </div>

            </div>
        </div>
    </div>
</aside>

<a href="#" data-toggle="push-menu" class="paper-nav-toggle left ml-2 fixed">
    <i></i>
</a>