@extends('admin.base')

@section('content')






    <div class="container-fluid my-3">
        <div class="d-flex row">
            <div class="col-md-7">
                <!-- Basic Validation -->
                <div class="card mb-3 shadow no-b r-0">
                    <div class="card-header white">
                        <h6>Create Student</h6>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate
                              action="{{route('web.admin.student.save')}}"
                              method="post"
                        >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01"> Name</label>
                                    <input name="name" type="text" class="form-control" id="validationCustom01" placeholder=" Name"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Class</label>
                                    <input type="text" name="class" class="form-control" id="validationCustom02" placeholder="Class"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Roll</label>
                                    <div class="input-group">

                                        <input type="text" name="roll" class="form-control" placeholder="Roll" aria-describedby="inputGroupPrepend" required>

                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01"> Section</label>
                                    <input name="section" type="text" class="form-control" id="validationCustom01" placeholder="Section"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Stoppage</label>
                                    <input type="text" name="stoppage" class="form-control" id="validationCustom02" placeholder="Stoppage"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Blood Group</label>
                                    <div class="input-group">

                                        <input type="text" name="blood_group" class="form-control" placeholder="Blood Group" aria-describedby="inputGroupPrepend" required>

                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01"> Mobile No</label>
                                    <input name="mobile" type="text" class="form-control" id="validationCustom01" placeholder="Mobile No"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Email</label>
                                    <input type="email" name="email" class="form-control" id="validationCustom02" placeholder="Email"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Password</label>
                                    <div class="input-group">

                                        <input type="password" name="password" class="form-control" placeholder="Password" aria-describedby="inputGroupPrepend" required>

                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01"> street</label>
                                    <input name="street" type="text" class="form-control" id="validationCustom01" placeholder="street"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">locality</label>
                                    <input type="text" name="locality" class="form-control" id="validationCustom02" placeholder="locality"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">city</label>
                                    <div class="input-group">

                                        <input type="text" name="city" class="form-control" placeholder="city" aria-describedby="inputGroupPrepend" required>

                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01"> Bus Id</label>
                                    <select name="bus_number" class="form-control">

                                        @foreach($bus as $b)

                                            <option value="{{$b->id}}">{{$b->name}}</option>
                                            @endforeach



                                    </select>

                                </div>

                            </div>






                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                    <label class="form-check-label" for="invalidCheck">
                                        Agree to terms and conditions
                                    </label>
                                    <div class="invalid-feedback">
                                        You must agree before submitting.
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>


                    </div>
                </div>
                <!-- #END# Basic Validation -->

                <!-- #END# Supported Elements -->
            </div>
            <div class="col-md-5">
                <h3>Buses</h3>

                <i style="size: 64px!important;" class="icon-bus"></i>

                <hr>
                <p>
                    Here you can create new buses
                </p>

                <hr>
            </div>
        </div>
    </div>


@endsection

