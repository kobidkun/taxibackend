@extends('admin.base')

@section('content')






    <div class="container-fluid my-3">
        <div class="d-flex row">
            <div class="col-md-7">
                <!-- Basic Validation -->
                <div class="card mb-3 shadow no-b r-0">
                    <div class="card-header white">
                        <h6>Create Bus</h6>
                    </div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate
                              action="{{route('web.admin.bus.save')}}"
                              method="post"
                        >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom01">Bus Name</label>
                                    <input name="name" type="text" class="form-control" id="validationCustom01" placeholder="Bus Name"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Bus Number</label>
                                    <input type="text" name="bus_number" class="form-control" id="validationCustom02" placeholder="Bus Number"  required>

                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustomUsername">Email</label>
                                    <div class="input-group">

                                        <input type="email" name="email" class="form-control" placeholder="Email" aria-describedby="inputGroupPrepend" required>

                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom03">Driver Name</label>
                                    <input type="text" name="driver_name" class="form-control" id="validationCustom03" placeholder="Driver Name" required>

                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom03">Password</label>
                                    <input type="password" name="password" class="form-control" id="validationCustom03" placeholder="Driver Name" required>

                                </div>


                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                    <label class="form-check-label" for="invalidCheck">
                                        Agree to terms and conditions
                                    </label>
                                    <div class="invalid-feedback">
                                        You must agree before submitting.
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>


                    </div>
                </div>
                <!-- #END# Basic Validation -->

                <!-- #END# Supported Elements -->
            </div>
            <div class="col-md-5">
                <h3>Buses</h3>

                <i style="size: 64px!important;" class="icon-bus"></i>

                <hr>
                <p>
                    Here you can create new buses
                </p>

                <hr>
            </div>
        </div>
    </div>


@endsection

